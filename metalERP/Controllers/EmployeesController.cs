﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using metalERP.Models.ViewModel;
using metalERP.Models.EntityManager;

namespace metalERP.Controllers
{
    public class EmployeesController : Controller
    {
        // GET: Employees
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListEmployeesPartial()
        {
            EmployeManager EM = new EmployeManager();
            List <EmployeData> employees = EM.GetAllEmployees();

            return PartialView(employees);
        }
    }
}