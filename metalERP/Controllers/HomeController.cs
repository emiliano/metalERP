﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using metalERP.Models.ViewModel;
using metalERP.Models.EntityManager;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace metalERP.Controllers
{
    public class HomeController : Controller
    {
        
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestPartial()
        {
            EmployeManager EM = new EmployeManager();
            EmployeDataView employees = new EmployeDataView();

            employees.EployeesInfo = EM.GetAllEmployees();

            return PartialView(employees);
        }

        public ActionResult FiltersForReport()
        {
            return PartialView();
        }

        public ActionResult ListByDepartment(string deptoName = "All")
        {
            EmployeManager EM = new EmployeManager();
            EmployeDataView employees = new EmployeDataView();
            DeptoName dep = new DeptoName();
            deptoName = deptoName == "All" ? "" : deptoName;
            employees.EployeesInfo = EM.GetByDepartment(deptoName);
            employees.nameDep = deptoName;
            //dep.nameDep = deptoName;
            return View(employees);
        }

        public ActionResult EmployeesByDepartment(string deptoName = "All")
        {
            EmployeManager EM = new EmployeManager();
            EmployeDataView employees = new EmployeDataView();
            deptoName = deptoName == "All" ? "" : deptoName;
            employees.EployeesInfo = EM.GetByDepartment(deptoName);

            return PartialView(employees);
        }

        public FileResult CreatePdf(string depto = "")
        {
           
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;

            string strPDFFileName = string.Format("ReportPDF{0}-.pdf", dTime.ToString("yyyyMMdd"));
            Document doc = new Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            PdfPTable tableLayout = new PdfPTable(5);
            doc.SetMargins(0f, 0f, 0f, 0f);
            string strAttachment = Server.MapPath("~/Downloadss/" + strPDFFileName);

            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            doc.Add(Add_Content_To_PDF(tableLayout, depto));
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            return File(workStream, "application/pdf", strPDFFileName);
        }

        #region Metodos para crear PDF
        protected PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, string filter)
        {
            float[] headers = { 50, 24, 45, 35, 50 };
            tableLayout.SetWidths(headers);
            tableLayout.WidthPercentage = 100;
            tableLayout.HeaderRows = 1;

            EmployeManager EM = new EmployeManager();
            EmployeDataView employees = new EmployeDataView();
            employees.EployeesInfo = EM.GetByDepartment(filter);

            tableLayout.AddCell(new PdfPCell(new Phrase("Report Employees", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0,0,0)))){
                Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER
            });

            AddCellToHeader(tableLayout, "Employe ID");
            AddCellToHeader(tableLayout, "First Name");
            AddCellToHeader(tableLayout, "Last Name");
            AddCellToHeader(tableLayout, "Initials");
            AddCellToHeader(tableLayout, "Department");

            foreach (var item in employees.EployeesInfo)
            {
                AddCellToBody(tableLayout, item.EmployeID);
                AddCellToBody(tableLayout, item.FirstName);
                AddCellToBody(tableLayout, item.LasttName);
                AddCellToBody(tableLayout, item.Initials);
                AddCellToBody(tableLayout, item.Department);
            }

            return tableLayout;
        }

        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.WHITE)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(128, 0, 0)
            });
        }

        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255)
            });
        }
        #endregion
    }
}