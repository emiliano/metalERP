﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace metalERP.Models.ViewModel
{
    public class EmployeModel
    {
    }

    public class EmployeDataModel
    {
        [Key]
        public string EmployeID { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LasttName { get; set; }
        [Display(Name = "Initials")]
        public string Initials { get; set; }
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Active")]
        public bool Active { get; set; }
        [Display(Name = "Department")]
        public string Department { get; set; }
    }

    public class EmployeData
    {
        [Key]
        public string EmployeID { get; set; }
        public string FirstName { get; set; }
        public string LasttName { get; set; }
        public string Initials { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
        public string Department { get; set; }
    }

    public class NameEmploye
    {
       
        public string FirstName { get; set; }
        public string LasttName { get; set; }
        public string Initials { get; set; }
    }

    public class EmployeNamesView
    {
        public IEnumerable<NameEmploye> EployeesInfo { get; set; }
    }


    public class EmployeDataView
    {
        public string nameDep { get; set; }
        public IEnumerable<EmployeData> EployeesInfo { get; set; }
    }

    public class DepartamentInfo
    {
        public int idDep { get; set; }
        public string nameDep { get; set; }
    }

    public class DeptoName
    {
        public string nameDep { get; set; }
    }

    public class DepartmentEmployeeView
    {
        //[Display(Name = "Department")]
        //public int DepartmentID { get; set; }
        public List<DepartamentInfo> DeprtmentNames { get; set; }
    }
}