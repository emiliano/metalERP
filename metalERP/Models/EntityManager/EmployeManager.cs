﻿using metalERP.Models.DB;
using metalERP.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace metalERP.Models.EntityManager
{
    public class EmployeManager
    {
        public List<EmployeData> GetAllEmployees(bool onlyactives = false)
        {
            using (acEntities db = new acEntities())
            {
                if (onlyactives)
                {
                    var employees = db.Employee_List.Where(e => e.Active.Equals(true))
                        .Select(e => new EmployeData
                        {
                            EmployeID = e.Employee_ID_,
                            FirstName = e.FirstName,
                            LasttName = e.LastName,
                            Initials = e.Initials,
                            Title = e.Title,
                            Active = e.Active,
                            Department = e.Department
                        }).ToList();
                    return employees;
                }
                else
                {
                    var employees = db.Employee_List.Select(e => new EmployeData
                    {
                        EmployeID = e.Employee_ID_,
                        FirstName = e.FirstName,
                        LasttName = e.LastName,
                        Initials = e.Initials,
                        Title = e.Title,
                        Active = e.Active,
                        Department = e.Department
                    }).ToList();
                    return employees;
                }

            }
        }

        public List<EmployeData> GetByDepartment(string department)
        {
            using (acEntities db = new acEntities())
            {
                if (string.IsNullOrEmpty(department))
                {
                    var result = db.Employee_List.Select(
                        k => new EmployeData
                        {
                            EmployeID = k.Employee_ID_,
                            FirstName = k.FirstName,
                            LasttName = k.LastName,
                            Initials = k.Initials,
                            Title = k.Title,
                            Active = k.Active,
                            Department = k.Department
                        }).ToList();
                    return result;
                }
                else
                {
                    var result = db.Employee_List.Where(a => a.Department.Equals(department)).Select(
                        k => new EmployeData
                        {
                            EmployeID = k.Employee_ID_,
                            FirstName = k.FirstName,
                            LasttName = k.LastName,
                            Initials = k.Initials,
                            Title = k.Title,
                            Active = k.Active,
                            Department = k.Department
                        }).ToList();
                    return result;
                }
            }
        }

        

        public List<DepartamentInfo> GetAllDeps()
        {
            using (acEntities db = new acEntities())
            {
                var deps = db.Employee_List.Select(a => new DepartamentInfo
                {
                    idDep = 1,
                    nameDep = a.Department
                }).ToList();
                return deps;
            }
        }

    }
}